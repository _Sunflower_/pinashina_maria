using System;

// Общий интерфейс для пиццы
public interface IPizza
{
    string GetDescription();
}

// Конкретная реализация пиццы
public class Pizza : IPizza
{
    private string size;
    private string crustType;
    private string toppings;

    public Pizza(string size, string crustType, string toppings)
    {
        this.size = size;
        this.crustType = crustType;
        this.toppings = toppings;
    }

    public string GetDescription()
    {
        return $"Size: {size}, Crust Type: {crustType}, Toppings: {toppings}";
    }
}

public abstract class PizzaFactory
{
    public abstract IPizza CreatePizza();
}

// Конкретная фабрика для создания Margherita Pizza
public class MargheritaPizzaFactory : PizzaFactory
{
    public override IPizza CreatePizza()
    {
        return new Pizza("Large", "Thin Crust", "Cheese, Tomato");
    }
}

// Конкретная фабрика для создания Pepperoni Pizza
public class PepperoniPizzaFactory : PizzaFactory
{
    public override IPizza CreatePizza()
    {
        return new Pizza("Medium", "Thick Crust", "Cheese, Pepperoni, Bell Peppers");
    }
}


// Абстрактный класс декоратора пиццы
public abstract class PizzaDecorator : IPizza
{
    protected IPizza pizza;

    public PizzaDecorator(IPizza pizza)
    {
        this.pizza = pizza;
    }

    public virtual string GetDescription()
    {
        return pizza.GetDescription();
    }
}

// Конкретный декоратор для добавления дополнительного ингредиента (Extra Cheese)
public class ExtraCheeseDecorator : PizzaDecorator
{
    public ExtraCheeseDecorator(IPizza pizza) : base(pizza)
    {
    }

    public override string GetDescription()
    {
        string description = base.GetDescription();
        description += ", Extra Cheese";
        return description;
    }
}

// Интерфейс команды
public interface ICommand
{
    void Execute();
}

// команда для выполнения заказа пиццы
public class OrderCommand : ICommand
{
    private IPizza pizza;

    public OrderCommand(IPizza pizza)
    {
        this.pizza = pizza;
    }

    public void Execute()
    {
        Console.WriteLine("Order placed for a pizza with the following details:");
        Console.WriteLine(pizza.GetDescription());
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        // Создание фабрик и объектов пиццы
        PizzaFactory margheritaPizzaFactory = new MargheritaPizzaFactory();
        IPizza margheritaPizza = margheritaPizzaFactory.CreatePizza();

        PizzaFactory pepperoniPizzaFactory = new PepperoniPizzaFactory();
        IPizza pepperoniPizza = pepperoniPizzaFactory.CreatePizza();

        // Создание и выполнение команды
        OrderCommand orderCommand = new OrderCommand(margheritaPizza);
        orderCommand.Execute();

        OrderCommand orderCommand2 = new OrderCommand(pepperoniPizza);
        orderCommand2.Execute();

        // Применение декоратора
        IPizza decoratedPizza = new ExtraCheeseDecorator(margheritaPizza);
    }
}
